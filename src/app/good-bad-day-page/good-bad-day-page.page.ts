import { Question } from './../providers/film.model';
import { FilmService } from './../providers/film.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FilmModel, FilmsModel } from '../providers/film.model';

@Component({
  selector: 'app-good-bad-day-page',
  templateUrl: './good-bad-day-page.page.html',
  styleUrls: ['./good-bad-day-page.page.scss'],
})
export class GoodBadDayPagePage implements OnInit {

  firstQuestion: Question = {
    question: '',
    type: ''
  };
  types = '';
  init = 0;
  mood: string;
  answer: boolean;

  nextQuestion: Question = {
    question: '',
    type: ''
  };

  films: FilmsModel = {
    code: 0,
    status: '',
    result: [{
      image: '',
      name: '',
      filmGenre: [],
      country: '',
      releaseDate: '',
      mood: [],
      streaming: [],
      plot: '',
      runningTime: 0,
    }]
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private filmService: FilmService) { }

  ngOnInit() {
    this.loadPage();
  }

  loadPage() {
    this.route.params.subscribe(
      (param) => {
        this.mood = this.router.getCurrentNavigation().extras.state.mood;
      });

    if (this.mood.startsWith('bene')) {
      this.firstQuestion.question = 'Sono molto contento per te! Vuoi rilassarti un po\'?';

    } else {
      this.firstQuestion.question = 'Può capitare una giornata no! Vuoi vedere un bel film '
        + 'che ti metta di buon umore per scaricare questo momento?';

    }

  }

  getFilmBymood(type: boolean) {
    this.films.result.pop();
    this.filmService.getFilm(this.mood, type)
      .subscribe(
        (res: FilmsModel) => {
          res.result.forEach(element => {
            this.films.result.push(element);
          });
          if (type) {
            this.nextQuestion.question = 'Sei un romanticone?';
            this.nextQuestion.type = 'Sentimentale';
            this.types = 'Sentimentale';
          } else {
            this.nextQuestion.question = 'Vuoi vedere un film movimentato?';
            this.nextQuestion.type = 'Azione';
          }
        });
    document.getElementById('first').style.visibility = 'collapse';
    document.getElementById('next').style.visibility = 'visible';
  };

  select(answer: boolean, type: string) {
    this.play(answer, this.nextQuestion.type);

    switch (type) {

      case 'Sentimentale':
        if (answer) {

          if (this.types.startsWith('Azione')) {
            this.nextQuestion.question = 'Un film adolescenziale potrebbe fare al caso tuo?';
            this.nextQuestion.type = 'Adolescenti';
          } else {
            this.nextQuestion.question = 'Vuoi che ci sia un po\' di comicità?';
            this.nextQuestion.type = 'Comico';
            this.types = 'Sentimentale';
          }
        } else {
          if (this.types.startsWith('Azione')) {
            this.nextQuestion.question = 'Vuoi che ti faccia ragionare sull\'importanza della famiglia?';
            this.nextQuestion.type = 'Motivazionale';
            this.types = 'Famiglia';
          } else if (this.types.startsWith('Sentimentale')) {
            this.nextQuestion.question = 'Vuoi che ti faccia scendere una lacrimuccia?';
            this.nextQuestion.type = 'Drammatico';
          } else {
            this.nextQuestion.question = 'Vuoi che ci sia un po\' di comicità?';
            this.nextQuestion.type = 'Comico';
          }
        }
        break;

      case 'Comico':
        if (answer) {
          if (this.types.startsWith('Dramm')) {
            this.nextQuestion.question = 'Ti piacciono i film con i balli';
            this.nextQuestion.type = 'Musicale';
          } else {
            this.nextQuestion.question = 'Che ne dici di partire domani con una grande carica motivazionale ?';
            this.nextQuestion.type = 'Motivazionale';
          }

        } else {
          if (this.types.startsWith('Sentim')) {
            this.nextQuestion.question = 'Vuoi vedere uno di quei film anni 80?';
            this.nextQuestion.type = 'Datato';
          } else {
            this.nextQuestion.question = 'Vuoi che ti faccia scendere una lacrimuccia?';
            this.nextQuestion.type = 'Drammatico';
          }
        }
        break;

      case 'Motivazionale':
        if (answer) {
          this.nextQuestion.question = '';
          this.nextQuestion.type = '';
        } else {
          if (this.types.startsWith('Famiglia')) {
            this.nextQuestion.question = 'Vuoi vedere uno di quei film anni 80?';
            this.nextQuestion.type = 'Datato';

          } else {

            this.nextQuestion.question = 'Vuoi che ti faccia scendere una lacrimuccia?';
            this.nextQuestion.type = 'Drammatico';
          }
        }
        break;

      case 'Drammatico':
        if (answer) {
          this.nextQuestion.question = 'Vuoi vedere uno di quei film anni 80?';
          this.nextQuestion.type = 'Datato';
        } else {
          this.nextQuestion.question = 'Vuoi che ci sia un po\' di comicità';
          this.nextQuestion.type = 'Comico';
          this.types = 'Drammatico';
        }
        break;

      case 'Musicale':
        if (!answer) {
          this.nextQuestion.question = 'Vuoi che ci sia un po\' d\'azione?';
          this.nextQuestion.type = 'Azione';
        }
        break;

      case 'Datato':
        if (answer) {
          this.nextQuestion.question = 'Sei uno spirito libero?';
          this.nextQuestion.type = 'Drammatico';
        }
        break;

      case 'Azione':
        if (answer) {
          this.nextQuestion.question = 'Sei un appassionato di film storici?';
          this.nextQuestion.type = 'Storico';
        } else {
          this.nextQuestion.question = 'Sei un romanticone?';
          this.nextQuestion.type = 'Sentimentale';
          this.types = 'Azione';
        }
        break;

      case 'Storico':
        if (answer) {
          this.nextQuestion.question = 'Vuoi che ci siano dei combattimenti di guerra?';
          this.nextQuestion.type = 'Guerra';
        } else {
          this.nextQuestion.question = 'Ti piace la fantascienza?';
          this.nextQuestion.type = 'Fantascienza';
        }
        break;

      case 'Fantascienza':
        if (!answer) {
          this.nextQuestion.question = 'Vuoi che ti tenga sulle spine?';
          this.nextQuestion.type = 'Thriller';
        } ;
        break;

      case 'Thriller':
        if (answer) {
          this.nextQuestion.question = '';
          this.nextQuestion.type = 'Thriller';
        } else {
          this.nextQuestion.question = '';
          this.nextQuestion.type = 'Thriller';
        }
        break;
    }

  }
  play(answer: boolean, type: string) {
    const database = this.films.result;
    const filmToRemove = [];

    if (database == null) {
      console.error('Controllare che i dati siano stati passati correttamente');
    }
    else {

      //In base alla risposta vera o falsa inserisce il film nell'array dei film da rimuovere
      database.forEach(element => {
        if (answer) {
          if (!element.filmGenre.includes(type)) {
            filmToRemove.push(element);
          }
        } else {
          if (element.filmGenre.includes(type)) {
            filmToRemove.push(element);
          }
        }
      });

      //Per tutti gli elementi inseriti nell'array da rimuovere, rimuove gli elementi nel database
      filmToRemove.forEach(element => {
        const index: number = database.indexOf(element);
        if (index !== -1) {
          database.splice(index, 1);
        }
      });

      //Se il database contiene un element allora l'algoritmo ha trovato la soluzione ottima
      if (database.length === 1) {
        const navigationExtras: NavigationExtras = {
          state: {
            film: database[0]
          }
        };
        this.router.navigate(['./end-game-page'], navigationExtras);
      };

    }
  }
}
