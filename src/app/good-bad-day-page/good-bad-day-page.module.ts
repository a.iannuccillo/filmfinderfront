import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodBadDayPagePageRoutingModule } from './good-bad-day-page-routing.module';

import { GoodBadDayPagePage } from './good-bad-day-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodBadDayPagePageRoutingModule
  ],
  declarations: [GoodBadDayPagePage]
})
export class GoodBadDayPagePageModule {}
