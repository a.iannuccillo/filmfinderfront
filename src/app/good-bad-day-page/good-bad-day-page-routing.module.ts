import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodBadDayPagePage } from './good-bad-day-page.page';

const routes: Routes = [
  {
    path: '',
    component: GoodBadDayPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodBadDayPagePageRoutingModule {}
