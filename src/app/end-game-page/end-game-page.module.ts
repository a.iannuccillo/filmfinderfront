import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndGamePagePageRoutingModule } from './end-game-page-routing.module';

import { EndGamePagePage } from './end-game-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndGamePagePageRoutingModule
  ],
  declarations: [EndGamePagePage]
})
export class EndGamePagePageModule {}
