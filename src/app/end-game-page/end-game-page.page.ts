import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FilmModel, FilmsModel } from '../providers/film.model';

@Component({
  selector: 'app-end-game-page',
  templateUrl: './end-game-page.page.html',
  styleUrls: ['./end-game-page.page.scss'],
})
export class EndGamePagePage implements OnInit {


  length: number;
  film: FilmModel = {
    code: 0,
    status: '',
    result: {
      image: '',
      name: 'TITOLO',
      filmGenre: [],
      country: '',
      releaseDate: '',
      mood: [],
      streaming: [],
      plot: '',
      runningTime: 0,
    }
  };

  constructor(private router: Router,
    private route: ActivatedRoute,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (param) => {
        this.film.result = this.router.getCurrentNavigation().extras.state.film;
      });
      console.log(this.film.result);
      this.length = 100/this.film.result.streaming.length;
      console.log(this.length);

  }

  goHome(){
    this.router.navigate(['./home']);
  }

}
