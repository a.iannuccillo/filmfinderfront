import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndGamePagePage } from './end-game-page.page';

const routes: Routes = [
  {
    path: '',
    component: EndGamePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndGamePagePageRoutingModule {}
