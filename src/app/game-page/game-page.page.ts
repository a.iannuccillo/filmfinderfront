import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.page.html',
  styleUrls: ['./game-page.page.scss'],
})
export class GamePagePage implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  goGame(mood: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        mood
      }
    };

    if (mood.startsWith('bene') || mood.startsWith('male')) {
      this.router.navigate(['./good-bad-day-page'], navigationExtras);
    } else {
      this.router.navigate(['./stressed-day-page'], navigationExtras);
    };
  }

}
