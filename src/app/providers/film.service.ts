import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  url = 'https://449b2316f072.ngrok.io/api/film/';
  httpOptions = {
    headers: new HttpHeaders({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'Content-type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getFilm(mood: string, answer: boolean) {
    const body = {
      mood,
      answer
    };

    return this.http.post(this.url + 'findFilmByMood', JSON.stringify(body), this.httpOptions);
  }

}
