export interface FilmModel {
  code: number;
  status: string;
  result: {
    image: string;
    name: string;
    filmGenre: string[];
    country: string;
    releaseDate: string;
    mood: string[];
    streaming: string[];
    plot: string;
    runningTime: number;
  };
}

export interface FilmsModel {
  code: number;
  status: string;
  result: [{
    image: string;
    name: string;
    filmGenre: string[];
    country: string;
    releaseDate: string;
    mood: string[];
    streaming: string[];
    plot: string;
    runningTime: number;
  }];
}
export interface Question {
  question: string;
  type: string;
}
