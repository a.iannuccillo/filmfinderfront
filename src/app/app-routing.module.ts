import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'game-page',
    loadChildren: () => import('./game-page/game-page.module').then( m => m.GamePagePageModule)
  },
  {
    path: 'good-bad-day-page',
    loadChildren: () => import('./good-bad-day-page/good-bad-day-page.module').then( m => m.GoodBadDayPagePageModule)
  },
  {
    path: 'stressed-day-page',
    loadChildren: () => import('./stressed-day-page/stressed-day-page.module').then( m => m.StressedDayPagePageModule)
  },
  {
    path: 'end-game-page',
    loadChildren: () => import('./end-game-page/end-game-page.module').then( m => m.EndGamePagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
