import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FilmsModel, Question } from '../providers/film.model';
import { FilmService } from '../providers/film.service';

@Component({
  selector: 'app-stressed-day-page',
  templateUrl: './stressed-day-page.page.html',
  styleUrls: ['./stressed-day-page.page.scss'],
})
export class StressedDayPagePage implements OnInit {
  firstQuestion: Question = {
    question: '',
    type: ''
  };
  types = '';
  init = 0;
  mood: string;
  answer: boolean;

  nextQuestion: Question = {
    question: '',
    type: ''
  };

  films: FilmsModel = {
    code: 0,
    status: '',
    result: [{
      image: '',
      name: '',
      filmGenre: [],
      country: '',
      releaseDate: '',
      mood: [],
      streaming: [],
      plot: '',
      runningTime: 0,
    }]
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private filmService: FilmService) { }

  ngOnInit() {
    this.loadPage();
  }

  loadPage() {
    this.films.result.pop();
    this.filmService.getFilm('Stressato', true)
      .subscribe(
        (res: FilmsModel) => {
          res.result.forEach(element => {
            this.films.result.push(element);
          });
          console.log(this.films.result.length);
        });

    this.nextQuestion.question = 'Vuoi vedere un bel film movimentato?';
    this.nextQuestion.type = 'Azione';
  }

  select(answer: boolean, type: string) {
    this.play(answer, this.nextQuestion.type);

    switch (type) {
      case 'Azione':
        if (answer) {
          this.nextQuestion.question = 'Sei un appassionato del Marvel cinematic universe?';
          this.nextQuestion.type = 'Fantascienza';
        } else {
          this.nextQuestion.question = 'Vuoi vedere un film con musica che ti dia la carica?';
          this.nextQuestion.type = 'Musicale';
        }
        break;

      case 'Musicale':
        if (answer) {
          this.nextQuestion.question = 'Sei un appassionato del Marvel cinematic universe?';
          this.nextQuestion.type = 'Fantascienza';
        } else {
          this.nextQuestion.question = 'Ti vuoi rilassare con un film romantico?';
          this.nextQuestion.type = 'Sentimentale';
        }
        break;

        case 'Sentimentale':
        if (answer) {
          this.nextQuestion.question = 'Che ne dici di partire domani con una grande carica motivazionale ?';
          this.nextQuestion.type = 'Motivazionale';
        } else {
          this.nextQuestion.question = '';
          this.nextQuestion.type = '';
        }
        break;

        case 'Motivazionale':
        if (answer) {
          this.nextQuestion.question = '';
          this.nextQuestion.type = '';
        } else {
          this.nextQuestion.question = 'Vuoi che ti faccia scendere una lacrimuccia?';
          this.nextQuestion.type = 'Drammatico';
        }
        break;
    }
  }

  play(answer: boolean, type: string) {
    const database = this.films.result;
    const filmToRemove = [];

    database.forEach(element => {
      if (answer) {
        if (!element.filmGenre.includes(type)) {
          filmToRemove.push(element);
          console.log('Database dentro ciclo db', database);
          console.log('Eliminati', filmToRemove);
        }
      } else {
        if (element.filmGenre.includes(type)) {
          filmToRemove.push(element);
        }
      }
    });

    filmToRemove.forEach(element => {
      const index: number = database.indexOf(element);
      if (index !== -1) {
        database.splice(index, 1);
        console.log(database);
      }
    });
    console.log('Database', database);
    console.log('Eliminati', filmToRemove);
    if (database.length <= 1) {
      const navigationExtras: NavigationExtras = {
        state: {
          film: database[0]
        }
      };
      this.router.navigate(['./end-game-page'], navigationExtras);
    };
  }

}
