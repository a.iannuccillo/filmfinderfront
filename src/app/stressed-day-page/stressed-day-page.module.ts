import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StressedDayPagePageRoutingModule } from './stressed-day-page-routing.module';

import { StressedDayPagePage } from './stressed-day-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StressedDayPagePageRoutingModule
  ],
  declarations: [StressedDayPagePage]
})
export class StressedDayPagePageModule {}
